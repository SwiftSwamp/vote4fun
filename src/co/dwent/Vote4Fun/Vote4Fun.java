package co.dwent.Vote4Fun;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import java.util.Date;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class Vote4Fun extends JavaPlugin
implements Listener
{
	public Logger pluginLog = Logger.getLogger("Minecraft");
	public static Permission permissions = null;
	public static Economy econ = null;
	public static String logPath = "plugins" + File.separator + "Vote4Fun" + File.separator + "votes.log";
	public static logIt voteLog = null;

	public void onEnable()
	{
		if (!setupEconomy())
		{
			this.pluginLog.info("[" + getDescription().getName() + "] - Disabled due to no Vault dependency found!");
			getServer().getPluginManager().disablePlugin(this);
			return;
		}

		if (getServer().getPluginManager().getPlugin("Votifier") == null)
		{
			this.pluginLog.info("[" + getDescription().getName() + "] - Disabled due to no Votifier plugin found!");
			getServer().getPluginManager().disablePlugin(this);
			return;
		}
		getServer().getPluginManager().registerEvents(this, this);
		getConfig().options().copyDefaults(true);
		saveConfig();

		if ( !(new File(logPath).exists() ) ) {
			try {
				new File(logPath).createNewFile();
			} catch (IOException ex) {
				System.out.println("[Logging]" + ex.getMessage());
			}
		}

		voteLog = new logIt(logPath);


		this.pluginLog.info("[Vote4Fun v" + getDescription().getVersion() + "] has been enabled!");
	}

	public void onDisable()
	{
		voteLog.close();
		this.pluginLog.info("[Vote4Fun] has been disabled!");
	}

	@EventHandler
	public void onVotifierEvent(VotifierEvent event)
	{

		Vote vote = event.getVote();
		String service = vote.getServiceName();


		String voterName = vote.getUsername();
		if (voterName.equalsIgnoreCase("Test Notification")) {
			this.pluginLog.info("Votifier Received Test Notification");
			return;
		}

		if (getConfig().getBoolean("VoteLogging.Enabled"))
		{
			voteLog.write("Player: " + vote.getUsername() + " | Player IP: " + vote.getAddress() + " | Service: " + vote.getServiceName());
		}

		Player[] players = Bukkit.getOnlinePlayers();
		ArrayList<String> users = new ArrayList<String>();
		for (int i = 0; i < players.length; i++) {
			users.add(players[i].getName().toLowerCase());
		}
		    
	    for (Player player : Bukkit.getOnlinePlayers()) {
		      if (player.getName().equalsIgnoreCase(getName())) {
		        return;
		      }
		    }
		{
			Player player = Bukkit.getPlayerExact(voterName);
			if (getConfig().getBoolean("Rewards.Money.Enabled"))
			{
				Double giveMoney = Double.valueOf(getConfig().getDouble("Rewards.Money.Amount"));
				econ.depositPlayer(player.getName(), giveMoney.doubleValue());
			}

			if (getConfig().getBoolean("Rewards.Items.Enabled"))
			{
				if (getConfig().isList("Rewards.Items.List"))
				{
					List<String> items = getConfig().getStringList("Rewards.Items.List");

					int line = 1;
					Iterator<String> itemsIter = items.iterator();
					//for (String item : itemsIter)
					while (itemsIter.hasNext())
					{
						String item = itemsIter.next();


						boolean error = false;
						int itemID = 0; int dataID = 0; int amount = 0;

						if (item.contains("-"))
						{
							String[] itemInfo = item.split("-");
							String[] itemData = itemInfo[0].split(":");
							try {
								itemID = Integer.parseInt(itemData[0]);
							}
							catch (NumberFormatException e) {
								this.pluginLog.info("[Vote4Fun - Config:Items] Error at line " + line + "at Item ID!");
								error = true;
							}
							try {
								dataID = Integer.parseInt(itemData[1]);
							}
							catch (NumberFormatException e) {
								this.pluginLog.info("[Vote4Fun - Config:Items] Error at line " + line + "at Data ID!");
								error = false;
							}
							try {
								amount = Integer.parseInt(itemInfo[1]);
							}
							catch (NumberFormatException e) {
								this.pluginLog.info("[Vote4Fun - Config:Items] Error at line " + line + "at Item Amount!");
								error = false;
							}
						}
						else
						{
							String[] itemData = item.split(":");
							try {
								itemID = Integer.parseInt(itemData[0]);
							}
							catch (NumberFormatException e) {
								this.pluginLog.info("[Vote4Fun - Config:Items] Error at line " + line + "at Item ID!");
								error = true;
							}
							try {
								amount = Integer.parseInt(itemData[1]);
							}
							catch (NumberFormatException e) {
								this.pluginLog.info("[Vote4Fun - Config:Items] Error at line " + line + "at Item Amount!");
								error = false;
							}
						}

						if (!error)
						{
							player.getInventory().addItem(new ItemStack[] { new ItemStack(itemID, amount, (short)dataID) });
						}
						else this.pluginLog.info("[Vote4Fun - Config:Items] No item was given to " + player.getName() + ". There is a error in the config!");
						line++;
					}
				} else {
					this.pluginLog.info("[Vote4Fun - Config:Items] Found no items in list, but was enabled!");
				}
			}
			if (getConfig().getBoolean("Messages.Enabled"))
			{
				String broadCastMessage = getConfig().getString("Messages.Broadcast").replace("%service%", service).replace("%player%", voterName).replaceAll("(&([a-f0-9]))", "�$2");
				Bukkit.broadcastMessage(broadCastMessage);

				String playerOnlyMessage = getConfig().getString("Messages.PlayerOnly").replace("%service%", service).replace("%player%", voterName).replaceAll("(&([a-f0-9]))", "�$2");
				player.sendMessage(playerOnlyMessage);
			}
		} else {
			this.pluginLog.info("[Vote4Fun - Player Offline] Could not reward " + voterName + ". *he was not online!");
		} 
	}

	private Object getOnlinePlayers() {
			// TODO Auto-generated method stub
			return null;
		}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if (cmd.getName().equalsIgnoreCase("Vote4Fun") || cmd.getName().equalsIgnoreCase("v4f"))
		{
			if ((!sender.isOp()) && ((sender instanceof Player)))
			{
				sender.sendMessage(ChatColor.RED + "You do not have access to that command.");
				return true;
			}
			if (args.length != 1)
			{
				sender.sendMessage(ChatColor.RED + "[Vote4Fun] " + ChatColor.WHITE + "Usage: /Vote4Fun {reload}");
				return false;
			}
			if (args[0].equals("reload"))
			{
				reloadConfig();
				sender.sendMessage(ChatColor.RED + "[Vote4Fun] " + ChatColor.WHITE + "The plugin config has been reloaded!");
			}
			else
			{
				sender.sendMessage(ChatColor.RED + "[Vote4Fun] command:");
				sender.sendMessage(ChatColor.WHITE + "/Vote4Fun reload " + ChatColor.GRAY + "Will reload the plugin config.");
			}
			return true;
		}
		return false;
	}

	private boolean setupEconomy()
	{
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
		if (rsp == null) {
			return false;
		}
		econ = (Economy)rsp.getProvider();
		return econ != null;
	}





	private  class logIt
	{

		private String fileName;

		private FileWriter fstream;
		private BufferedWriter out; 

		public logIt(String filename)
		{
			this.fileName = filename;


			try
			{
				//fstream = new FileWriter(GroupBlacklist.logDir + this.fileName, true);
				fstream = new FileWriter(this.fileName, true);
				out = new BufferedWriter(fstream);
			}
			catch (IOException ex)
			{
				System.out.println("[Logging]" + ex.getMessage());
			}

		}

		public String date()
		{
			SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss");
			Date now = new Date();
			return format.format(now);
		}

		public String parseText(String input)
		{
			return input.replace('\'', '"');
		}


		public void write(String data)
		{
			try
			{
				out.write(date() + " | "  + parseText(data));
				out.newLine();
				out.flush();
			}
			catch (Exception ex)
			{
				System.out.println("[Logging]" + ex.getMessage());
			}

		}

		public void close()
		{
			try
			{
				out.close();
			}
			catch (IOException ex)
			{
				System.out.println("[Logging]" + ex.getMessage());
			}
		}

	}

}